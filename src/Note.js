import React, { Fragment } from "react";

import { Typography } from "@material-ui/core/";

const Note = ({ note: { title, description }}) => {
	return (
		<Fragment>
			<Typography align="center" variant="h4" gutterBottom>
				{title}
			</Typography>
			<Typography align="justify" variant="subtitle1">
				{description}
			</Typography>
		</Fragment>
	);
}

export default Note;
