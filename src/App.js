import React, { Fragment, Component } from "react";

/* Material-UI */
import { Typography, Grid, Fab } from "@material-ui/core/";
import { Add as AddIcon } from "@material-ui/icons/";

/* react-router */
import { Link, Route, Redirect } from "react-router-dom";

/* Notes Components */
import NotesForm from "./NotesForm.js";
import NotesList from "./NotesList.js";
import Home from "./Home.js";
import Note from "./Note.js";

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			title: "",
			description: "",
			notes: []
		}
	}

	updateField = (field) => (e) => {
		this.setState({
			[field]: e.target.value
		});
	}

	saveNote = () => {
		if (this.state.title.trim() && this.state.description.trim()) {
			/* By getting state as a param, we make sure we get the latest state content */
			this.setState(state => {
				return ({
					notes: [
						...state.notes, {
							id: Date.now(),
							title: state.title,
							description: state.description
						}
					],
					title: "",
					description: ""
				})
			});
		}
	}

	deleteNote = (id) => {
		this.setState({
			notes: this.state.notes.filter(note => note.id !== id)
		});
	}

	render() {
		return (
			<Fragment>
				<Typography align="center" variant="h2" gutterBottom>
					React - Notes
				</Typography>
				<Grid container justify="center" spacing={2}>
					<Grid item xs={4}>
						<NotesList deleteNote={this.deleteNote} notes={this.state.notes}/>
					</Grid>
					<Grid item xs={8}>
						<Route exact path="/" component={Home}/>
						<Route
							exact
							path="/addnote/"
							render={() => (
									<NotesForm
										updateField={this.updateField}
										saveNote={this.saveNote}
										title={this.state.title}
										description={this.state.description}
									/>
								)
							}
						/>
						<Route
							path="/view/:id/"
							render={(props) => {
								const note = this.state.notes.find(note => {
									return note.id === parseInt(props.match.params.id)
								});
								return (note) ? <Note note={note}/> : <Redirect to="/"/>;
							}}
						/>
					</Grid>
				</Grid>

				<Fab color="primary" component={Link} exact to="/addnote/" className="addIcon">
					<AddIcon />
				</Fab>
			</Fragment>
		);
	}
}

export default App;
