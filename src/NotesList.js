import React from "react";

import moment from "moment";

/* react-router */
import { Link } from "react-router-dom";

import { Typography } from "@material-ui/core/"
import { List } from "@material-ui/core/"
import { ListItem } from "@material-ui/core/"
import { ListItemSecondaryAction } from "@material-ui/core/"
import { ListItemText } from "@material-ui/core/"
import { IconButton } from "@material-ui/core/"
import DeleteIcon from "@material-ui/icons/Delete";

const NotesList = ({ notes, deleteNote }) => {
	return notes.length ? (
		<List>
			{
				notes.map((note) => {
					return (
						<ListItem button key={ note.id } component={ Link } to={ `/view/${note.id}` }>
							<ListItemText
								primary={ note.title }
								secondary={ moment(note.id).calendar() }
							/>
							<ListItemSecondaryAction>
								<IconButton onClick={() => { deleteNote(note.id) }} color="secondary">
									<DeleteIcon/>
								</IconButton>
							</ListItemSecondaryAction>
						</ListItem>
					)
				})
			}
		</List>
	) : (
		<Typography variant="h6" align="center">
			You don't have any notes yet :(
		</Typography>
	);
}

export default NotesList;
