import React from "react";

import { Typography } from "@material-ui/core/"

const Home = () => {
	return (
		<Typography align="center" variant="h4">
			Welcome Back!
		</Typography>
	);
}

export default Home;
