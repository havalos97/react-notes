import React, { Fragment } from "react";

import { TextField, Grid, Fab, Icon } from "@material-ui/core/";

const Notesform = ({ updateField, saveNote, title, description }) => {
	return (
		<Fragment>
			<Grid item xs={11}>
				<TextField
					onChange={ updateField("title") }
					type="text"
					label="Title"
					margin="normal"
					value={ title }
					fullWidth
				/>
			</Grid>
			<Grid item xs={11}>
				<TextField
					multiline
					rows="4"
					margin="normal"
					fullWidth
					placeholder="Start taking come notes from your heart..."
					value={ description }
					onChange={ updateField("description") }
				/>
			</Grid>
			<Fab color="secondary" className="editIcon" onClick={ saveNote }>
				<Icon>edit_icon</Icon>
			</Fab>
		</Fragment>
	);
}

export default Notesform;
